/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_individualactivity;

/**
 *
 * @author Lo, Lee Ann D. INF201
 */
public class OOP_IndividualActivity_Transportation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Toyota_VIOS obj = new Toyota_VIOS();
        System.out.println(obj.Name);
        System.out.println(obj.Tire_Type);
        obj.drive();
        obj.speed();
        obj.color();
        obj.price();
        System.out.println("\n");

        U2_Spy_Plane obj1 = new U2_Spy_Plane();
        System.out.println(obj1.Name);    
        System.out.println(obj1.Wing_Span);
        obj1.fly();
        obj1.speed();
        obj1.color();
        obj1.price();
        System.out.println("\n");

        Fandango obj2 = new Fandango();
        System.out.println(obj2.Name);
        System.out.println(obj2.MainSail_Color);
        obj2.Float();
        obj2.speed();
        obj2.color();
        obj2.price();
        System.out.println("\n");
       
        Vehicle obj3 = new Vehicle();
        obj3.stop();
    }    
}

class Vehicle{
    String speed;
    String color;
    String price;
    
    void stop(){
        System.out.println("STOP");
    }
}

class Toyota_VIOS extends Vehicle{
    String Name = "Toyota VIOS - Sedan Car";
    String Tire_Type = "Tire Type: ALL-SEASON: Touring tire is designed for reliable all-season traction.";
    
    public void drive(){
        System.out.println("I'm driving my Toyata VIOS!");
    }
    public void speed(){
        System.out.println("Maximum Speed: 170 kph");
    }
    public void color(){
        System.out.println("Color: Gray");
    }
    public void price(){
        System.out.println("Price: ₱1,020,000.00");
    }
}

class U2_Spy_Plane extends Vehicle{
    String Name = "U-2 Spy Plane - Aircraft";
    String Wing_Span = "Wing Span: 90";
    
    public void fly(){
        System.out.println("The Airplane is flying at an altitude of 33,000 ft.");
    }
    public void speed(){
        System.out.println("Speed: 2200 mph");
    }
    public void color(){
        System.out.println("Color: White, Blue, and Red");
    }
    public void price(){
        System.out.println("Price: $15,000");
    }
}

class Fandango extends Vehicle{
    String Name = "Fandango - Small Sea Vessel";
    String MainSail_Color = "Main Sail Color: Billowing Clouds\n" +
                            "PPG1041-3\n" +
                            "RGB\n" +
                            "216, 222, 227";
    
    public void Float(){
        System.out.println("My yacht is floating near the beach.");
    }
    public void speed(){
        System.out.println("Speed: 15 knots (17 mph or 28 km/h)");
    }
    public void color(){
        System.out.println("Color: White and Blue");
    }
    public void price(){
        System.out.println("Price: $300,000 - $8.4 million");
    }
}